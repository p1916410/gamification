# Gamification

Réalisé par Jeremy KRITIKOS - p2109032, Tom MONTAURIOL - p1916410

## Installation

Le projet utilise Python en version 3.9 ou supérieur.

Les dépendances sont:

* pandas
* scipy
* numpy

Pour les installer de manière automatique, il suffit de se placer à la racine du projet et d'éxécuter la commande :

```pip install -r requirements.txt```

Ensuite, le projet se lance comme un module Python (toujours depuis la racine du projet):

``python3 -m src``


