import os

import numpy as np
import pandas as pd

from scipy.stats import ttest_ind


def import_directory(directory_path):
    directory_path = os.path.normpath(directory_path)
    files = [os.path.join(directory_path, f) for f in os.listdir(directory_path) if f.endswith(".csv")]
    dictionary_coeff = {}
    dictionary_pvalues = {}

    for file in files:
        df = pd.read_csv(file, sep=';')
        if "pVals" in file:
            dictionary_pvalues[file] = df
        else:
            dictionary_coeff[file] = df

    return dictionary_coeff, dictionary_pvalues


def compute_score(influence: dict[str, float]) -> float:
    return influence["ME"] + influence["MI"] - influence["AM"]


def create_affinity_vector(affinity_dict: dict[str, float]):
    lt = list(affinity_dict.items())
    lt.sort(key=lambda x: x[1], reverse=True)
    return lt


def create_affinity_dict(influenceME, influenceMI, influenceAM, profil) -> dict[str, float]:
    features_utils = {}
    for feature in influenceME:
        features_utils[feature] = 0
        for elem_profil, value in profil.items():
            d_scores = {}
            for name, d in zip(("ME", "MI", "AM"), (influenceME, influenceMI, influenceAM)):
                d_scores[name] = value * d[feature][elem_profil]
            features_utils[feature] += compute_score(d_scores)
        features_utils[feature] = round(features_utils[feature], 2)
    return features_utils


def filter_p_values(dict_coeff, dict_pvalues):
    influenceME = {}
    influenceMI = {}
    influenceAM = {}

    for file, df in dict_coeff.items():
        for file_pvalues, df_pvalues in dict_pvalues.items():
            feature = os.path.split(file)[-1].split("Path")[0]
            if feature in file_pvalues:
                for rowIndex, row in df_pvalues.iterrows():
                    for columnIndex, value in row.items():
                        if isinstance(value, float):
                            if rowIndex == 0:
                                if feature not in influenceMI:
                                    influenceMI[feature] = {}
                                influenceMI[feature][columnIndex] = (
                                    df[columnIndex][rowIndex] if value < treshold_pvalue else 0)
                            if rowIndex == 1:
                                if feature not in influenceME:
                                    influenceME[feature] = {}
                                influenceME[feature][columnIndex] = (
                                    df[columnIndex][rowIndex] if value < treshold_pvalue else 0)
                            if rowIndex == 2:
                                if feature not in influenceAM:
                                    influenceAM[feature] = {}
                                influenceAM[feature][columnIndex] = (
                                    df[columnIndex][rowIndex] if value < treshold_pvalue else 0)
    return influenceMI, influenceME, influenceAM


def choose_feature(v_hexad, v_motivation, worst=False) -> str:
    # Parametres :
    facteur_hexad = 2
    facteur_motivation = 1

    indexes = {}
    for idx, v in enumerate([a for a, _ in v_hexad]):
        indexes[v] = (idx + 1) * facteur_hexad
    for idx, v in enumerate([a for a, _ in v_motivation]):
        indexes[v] += (idx + 1) * facteur_motivation

    return max(indexes, key=indexes.get) if worst else min(indexes, key=indexes.get)


def make_group_students(user_stats, feature):
    return user_stats.loc[user_stats.GameElement == user_stats[feature]]


def make_inadapted_group_students(user_stats, feature):
    return user_stats.loc[user_stats.GameElement != user_stats[feature]]


def t_test(a, b):
    ttest = ttest_ind(a, b)
    # print(ttest)
    print("pvalue ttest : ", round(ttest[1], 4))


def compare_means(a: list[float], b: list[float]):
    return round(np.mean(a), 2), round(np.mean(b), 2)


if __name__ == '__main__':

    # Partie 1 - Etape 2 :

    hexad_coeff, hexad_pvalues = import_directory("data/Hexad")
    motivation_coeff, motivation_pvalues = import_directory("data/Motivation")

    treshold_pvalue = 0.1

    user_stats = pd.read_csv("data/Données élèves/userStats.csv", sep=';')

    hexadMI, hexadME, hexadAM = filter_p_values(hexad_coeff, hexad_pvalues)
    motivationMI, motivationME, motivationAM = filter_p_values(motivation_coeff, motivation_pvalues)

    user_stats['best_feature'] = [''] * len(user_stats.User)
    user_stats['worst_feature'] = [''] * len(user_stats.User)

    for i, user in user_stats.iterrows():
        motivation_initiale = {"MI": user["micoI"] + user[" miacI"] + user[" mistI"],
                               "ME": user[" meidI"] + user[" meinI"] + user[" mereI"],
                               "amotI": user[" amotI"]}
        profil = dict(user[10:16])
        v1 = create_affinity_vector(create_affinity_dict(hexadME, hexadMI, hexadAM, profil))
        v2 = create_affinity_vector(create_affinity_dict(motivationME, motivationMI, motivationAM, motivation_initiale))

        # Partie 2 - Etape 3

        best_feature = choose_feature(v1, v2)
        worst_feature = choose_feature(v1, v2, True)

        # Partie 2 - Etape 4
        user_stats.loc[i, 'best_feature'] = best_feature
        user_stats.loc[i, 'worst_feature'] = worst_feature

    adapted_students = make_group_students(user_stats, 'best_feature')
    undapted_students = make_group_students(user_stats, 'worst_feature')
    # undapted_students = make_inadapted_group_students(user_stats, 'best_feature')

    a = pd.to_timedelta(pd.to_datetime(adapted_students['Time'])
                        .dt.strftime('%H:%M:%S')).dt.total_seconds().astype(int).to_list()

    b = pd.to_timedelta(pd.to_datetime(undapted_students['Time'])
                        .dt.strftime('%H:%M:%S')).dt.total_seconds().astype(int).to_list()
    ma, mb = compare_means(a, b)
    print("TIME")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = (adapted_students["CorrectCount"] / adapted_students["QuestionCount"]).to_list()
    b = (undapted_students["CorrectCount"] / undapted_students["QuestionCount"]).to_list()
    ma, mb = compare_means(a, b)
    print("POURCENTAGE BONNE REPONSES")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = adapted_students["QuestionCount"].to_list()
    b = undapted_students["QuestionCount"].to_list()
    ma, mb = compare_means(a, b)
    print("NOMBRE REPONSES")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = (adapted_students[" micoVar"] + adapted_students[" miacVar"] + adapted_students[" mistVar"]).to_list()
    b = (undapted_students[" micoVar"] + undapted_students[" miacVar"] + undapted_students[" mistVar"]).to_list()
    ma, mb = compare_means(a, b)
    print("VARIATION MOTIVATION INTRINSEQUE")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = (adapted_students[" meidVar"] + adapted_students[" meinVar"] + adapted_students[" mereVar"]).to_list()
    b = (undapted_students[" meidVar"] + undapted_students[" meinVar"] + undapted_students[" mereVar"]).to_list()
    ma, mb = compare_means(a, b)
    print("VARIATION MOTIVATION EXTRINSEQUE")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = adapted_students[" amotVar"].to_list()
    b = undapted_students[" amotVar"].to_list()
    ma, mb = compare_means(a, b)
    print("VARIATION AMOTIVATION")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = adapted_students["FullyCompletedLessonCount"].to_list()
    b = undapted_students["FullyCompletedLessonCount"].to_list()
    ma, mb = compare_means(a, b)
    print("FullyCompletedLessonCount")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = adapted_students["QuizCount"].to_list()
    b = undapted_students["QuizCount"].to_list()
    ma, mb = compare_means(a, b)
    print("QuizCount")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)

    a = adapted_students["PassedQuizCount"].to_list()
    b = undapted_students["PassedQuizCount"].to_list()
    ma, mb = compare_means(a, b)
    print("PassedQuizCount")
    print("Moyenne adapted students:", ma)
    print("Moyenne undapted students:", mb)
    t_test(a, b)
    print("*" * 50)
